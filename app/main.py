import logging
from logging.config import fileConfig

from fastapi import FastAPI
from starlette.staticfiles import StaticFiles

import models
from database import engine
from admin import admin
from routers import mailings, clients, messages, stats


logging.config.fileConfig('logging.conf', disable_existing_loggers=False)
logger = logging.getLogger(__name__)

app = FastAPI()

models.Base.metadata.create_all(bind=engine)

app.mount(
    "/admin/static",
    StaticFiles(directory="admin/static"),
    name="static",
)

routers = [
    mailings.router,
    clients.router,
    messages.router,
    stats.router,
    admin.router,
]

for router in routers:
    app.include_router(router)
