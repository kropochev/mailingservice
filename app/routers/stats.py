
from datetime import datetime, timedelta

from typing import Dict, List
from fastapi import Depends, APIRouter, status, HTTPException
from pydantic import BaseModel, Field
from sqlalchemy.orm import Session

import models
from database import engine, get_db


router = APIRouter(
    prefix="/stats",
    tags=["stats"],
    responses={404: {"description": "Not found"}}
)

models.Base.metadata.create_all(bind=engine)


class MailingBase(BaseModel):
    start: datetime = Field(title="Start of the mailing")
    stop: datetime = Field(title="End of the mailing")
    text: str = Field(
        title="Text of the mailing",
        min_length=1,
        max_length=1000
        )
    filter: Dict[str, List] = Field(title="Filter of the mailing")

    class Config:
        schema_extra = {
            "example": {
                "start": datetime.now(),
                "stop": datetime.now() + timedelta(days=1),
                "text": "Text of the mailing",
                "filter": {
                    "mobile_code": [900],
                    "tags": ["tag1", "tag2"],
                }
            }
        }


class MailingCreate(MailingBase):
    pass


class Mailing(MailingBase):
    id: int

    class Config:
        orm_mode = True


@router.get("/", status_code=status.HTTP_200_OK)
async def get_stat(db: Session = Depends(get_db)):
    mailing_list = db.query(models.Mailings).all()
    message_list = db.query(models.Messages).all()
    stats = {
        "mailings": len(mailing_list),
        "messages": len(message_list),
    }
    return stats


@router.get("/{status}", status_code=status.HTTP_200_OK)
async def get_message_by_status(status: str, db: Session = Depends(get_db)):
    message_list = db.query(models.Messages)\
        .filter(models.Messages.status == status)\
        .all()
    return message_list


def raise_mailing_not_found():
    return HTTPException(
        status_code=400,
        detail="Mailing not found",
        headers={"X-Header-Error": "Nothing to be seen"},
    )
