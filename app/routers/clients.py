import logging

from fastapi import Depends, APIRouter, status, HTTPException
from pydantic import BaseModel, Field, constr
from sqlalchemy.orm import Session

import models
from database import engine, SessionLocal


logger = logging.getLogger(__name__)

router = APIRouter(
    prefix="/clients",
    tags=["clients"],
    responses={404: {"description": "Not found"}}
)

models.Base.metadata.create_all(bind=engine)


def get_db():
    try:
        db = SessionLocal()
        yield db
    finally:
        db.close()


class ClientBase(BaseModel):
    phone_number: constr(regex=r"^7\d{10}$") = Field(
        title="Phone number of the client",
    )
    mobile_code: constr(regex=r"^\d{3}$") = Field(
        title="Mobile code of the client",
    )
    tag: str = Field(title="Tag of the client")
    time_zone: int = Field(title="Time zone of the client", gt=-12, lt=12)

    class Config:
        schema_extra = {
            "example": {
                "phone_number": 79001234567,
                "mobile_code": 900,
                "tag": "tag1",
                "time_zone": 3,
            }
        }


class ClientCreate(ClientBase):
    pass


class Client(ClientBase):
    id: int

    class Config:
        orm_mode = True


@router.post("/", status_code=status.HTTP_201_CREATED)
async def add_client(client: ClientCreate, db: Session = Depends(get_db)):

    client_model = models.Clients()
    client_model.phone_number = client.phone_number
    client_model.mobile_code = client.mobile_code
    client_model.tag = client.tag
    client_model.timezone = client.time_zone

    db.add(client_model)
    db.commit()

    logger.info("Create client id: %d", client_model.id)

    return {"message": "Client created"}


@router.put("/", status_code=status.HTTP_200_OK)
async def update_client(client: Client, db: Session = Depends(get_db)):
    client_model = (
        db.query(models.Clients)
        .filter(models.Clients.id == client.id)
        .first()
    )
    if client_model is None:
        raise raise_client_not_found()

    logger.info("Update client id: %d", client_model.id)

    client_model.phone_number = client.phone_number
    client_model.tag = client.tag
    client_model.timezone = client.time_zone

    db.add(client_model)
    db.commit()
    return {"message": "Client updated"}


@router.delete("/", status_code=status.HTTP_200_OK)
async def delete_client(client_id: int, db: Session = Depends(get_db)):
    client_model = (
        db.query(models.Clients)
        .filter(models.Clients.id == client_id)
        .first()
    )

    if client_model is None:
        raise raise_client_not_found()

    logger.info("Delete client id: %d", client_model.id)

    db.query(models.Clients).filter(
        models.Clients.id == client_id
    ).delete()
    db.commit()
    return {"message": "Client deleted"}


def raise_client_not_found():
    return HTTPException(
        status_code=400,
        detail="Client not found",
        headers={"X-Header-Error": "Nothing to be seen"},
    )
