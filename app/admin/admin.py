import json
import logging
from datetime import datetime, timedelta

from starlette.responses import RedirectResponse

from fastapi import APIRouter, Depends, Request, Response, Form, status
from fastapi.security import HTTPBearer
from fastapi.responses import HTMLResponse
from fastapi.templating import Jinja2Templates
from fastapi.encoders import jsonable_encoder

from sqlalchemy.orm import Session

import models
from worker import send_msg
from database import engine, get_db
from .utils import VerifyToken


logger = logging.getLogger(__name__)

token_auth_scheme = HTTPBearer()

router = APIRouter(
    prefix="/admin",
    tags=["admin"],
    responses={418: {"description": "Internal Use Only"}},
)

models.Base.metadata.create_all(bind=engine)

templates = Jinja2Templates(directory="admin/templates")


@router.get("/", response_class=HTMLResponse)
async def index(
    request: Request,
    response: Response,
    # token: str = Depends(token_auth_scheme),
    db: Session = Depends(get_db),
):
    logger.info("Open admin page")

    # TODO добавить OAuth2 авторизацию
    # result = VerifyToken(token.credentials).verify()

    # if result.get("status"):
    #     response.status_code = status.HTTP_400_BAD_REQUEST
    #     return result

    mailings = db.query(models.Mailings).all()
    messages = db.query(models.Messages).all()

    return templates.TemplateResponse(
        "home.html", {
            "request": request,
            "mailings": mailings,
            "messages": messages,
            }
    )


@router.get("/show-mailing/{id}", response_class=HTMLResponse)
async def show_mailing(
    request: Request,
    response: Response,
    id: int,
    # token: str = Depends(token_auth_scheme),
    db: Session = Depends(get_db),
):

    # result = VerifyToken(token.credentials).verify()

    # if result.get("status"):
    #     response.status_code = status.HTTP_400_BAD_REQUEST
    #     return result

    mailing = db.query(models.Mailings)\
        .filter(models.Mailings.id == id)\
        .first()

    return templates.TemplateResponse(
        "mailing.html", {"request": request, "mailing": mailing}
    )


@router.get("/show-client/{id}", response_class=HTMLResponse)
async def show_client(
    request: Request,
    response: Response,
    id: int,
    # token: str = Depends(token_auth_scheme),
    db: Session = Depends(get_db),
):

    # result = VerifyToken(token.credentials).verify()

    # if result.get("status"):
    #     response.status_code = status.HTTP_400_BAD_REQUEST
    #     return result

    client = db.query(models.Clients)\
        .filter(models.Clients.id == id)\
        .first()

    return templates.TemplateResponse(
        "client.html", {"request": request, "client": client}
    )


@router.get("/edit/{id}", response_class=HTMLResponse)
async def edit_mailing(
    request: Request,
    response: Response,
    id: int,
    # token: str = Depends(token_auth_scheme),
    db: Session = Depends(get_db),
):

    # result = VerifyToken(token.credentials).verify()

    # if result.get("status"):
    #     response.status_code = status.HTTP_400_BAD_REQUEST
    #     return result

    mailing = db.query(models.Mailings)\
        .filter(models.Mailings.id == id).first()

    return templates.TemplateResponse(
        "edit.html", {"request": request, "mailing": mailing}
    )


@router.post("/edit/{id}", response_class=HTMLResponse)
async def edit_mailing_commit(
    response: Response,
    id: int,
    text: str = Form(...),
    start: datetime = Form(...),
    stop: datetime = Form(...),
    filter: str = Form(...),
    # token: str = Depends(token_auth_scheme),
    db: Session = Depends(get_db),
):

    # result = VerifyToken(token.credentials).verify()

    # if result.get("status"):
    #     response.status_code = status.HTTP_400_BAD_REQUEST
    #     return result
    logger.info("Edit mailing id: %d", id)

    mailing_model = db.query(models.Mailings)\
        .filter(models.Mailings.id == id)\
        .first()

    mailing_model.start = start
    mailing_model.stop = stop
    mailing_model.text = text
    mailing_model.filter = jsonable_encoder(filter)

    db.add(mailing_model)
    db.commit()

    return RedirectResponse(url="/admin", status_code=status.HTTP_302_FOUND)


@router.get("/add", response_class=HTMLResponse)
async def add_mailing(
    request: Request,
    response: Response,
    # token: str = Depends(token_auth_scheme),
):

    # result = VerifyToken(token.credentials).verify()

    # if result.get("status"):
    #     response.status_code = status.HTTP_400_BAD_REQUEST
    #     return result

    start = datetime.now()
    stop = datetime.now() + timedelta(days=1)

    return templates.TemplateResponse(
        "add.html", {"request": request, "start": start, "stop": stop}
    )


@router.post("/add", response_class=HTMLResponse)
async def add_mailing_commit(
    response: Response,
    text: str = Form(...),
    start: datetime = Form(...),
    stop: datetime = Form(...),
    filter: str = Form(...),
    # token: str = Depends(token_auth_scheme),
    db: Session = Depends(get_db),
):

    # result = VerifyToken(token.credentials).verify()

    # if result.get("status"):
    #     response.status_code = status.HTTP_400_BAD_REQUEST
    #     return result

    mailing_model = models.Mailings()

    mailing_model.start = start
    mailing_model.stop = stop
    mailing_model.text = text
    mailing_model.filter = json.loads(filter)

    db.add(mailing_model)
    db.commit()

    logger.info("Add mailing id: %d", mailing_model.id)

    await send_msg(mailing_model.id)

    return RedirectResponse(url="/admin", status_code=status.HTTP_302_FOUND)


@router.get("/delete/{id}")
async def delete_mailing(
    response: Response,
    id: int,
    # token: str = Depends(token_auth_scheme),
    db: Session = Depends(get_db),
):

    # result = VerifyToken(token.credentials).verify()

    # if result.get("status"):
    #     response.status_code = status.HTTP_400_BAD_REQUEST
    #     return result

    mailing_model = db.query(models.Mailings)\
        .filter(models.Mailings.id == id)\
        .first()

    if mailing_model is None:
        return RedirectResponse(
            url="/admin",
            status_code=status.HTTP_302_FOUND,
        )

    logger.info("Delete mailing id: %d", id)

    db.query(models.Mailings).filter(models.Mailings.id == id).delete()
    db.commit()

    return RedirectResponse(url="/admin", status_code=status.HTTP_302_FOUND)
