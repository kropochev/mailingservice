from sqlalchemy import (
    Column,
    Integer,
    BigInteger,
    String,
    ForeignKey,
    JSON,
    DateTime,
)
from sqlalchemy.orm import relationship
from database import Base


class Mailings(Base):
    __tablename__ = 'mailings'

    id = Column(Integer, primary_key=True, index=True)
    start = Column(DateTime)
    stop = Column(DateTime)
    text = Column(String(250))
    filter = Column(JSON)

    messages = relationship("Messages", back_populates="mailings")


class Clients(Base):
    __tablename__ = 'clients'

    id = Column(Integer, primary_key=True, index=True)
    phone_number = Column(BigInteger)
    mobile_code = Column(Integer)
    tag = Column(JSON)
    timezone = Column(Integer)

    messages = relationship("Messages", back_populates="clients")


class Messages(Base):
    __tablename__ = 'messages'

    id = Column(Integer, primary_key=True, index=True)
    datetime = Column(DateTime)
    status = Column(String)
    mailing_id = Column(Integer, ForeignKey("mailings.id"))
    client_id = Column(Integer, ForeignKey("clients.id"))

    mailings = relationship("Mailings", back_populates="messages")
    clients = relationship("Clients", back_populates="messages")
