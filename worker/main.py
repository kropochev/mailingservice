import os
from datetime import datetime
from aiohttp import ClientSession
from arq.connections import RedisSettings

REDIS_HOST = os.environ.get('REDIS_HOST')
REDIS_PORT = os.environ.get('REDIS_PORT')
TOKEN = os.environ.get('TOKEN')
API_HOST = os.environ.get('API_HOST')
API_PORT = os.environ.get('API_PORT')


async def send_message(ctx, id, phone_number, text, data):
    headers = {
        "Authorization": f"Bearer {TOKEN}"
    }
    async with ClientSession(headers=headers) as session:

        url = f"https://probe.fbrq.cloud/v1/send/{id}"
        payload = {
            "id": id,
            "phone": phone_number,
            "text": text
        }
        async with session.post(url=url, json=payload) as response:
            if response.status == 200:
                await report_progress(status="delivered", data=data)
            else:
                await report_progress(status="not delivered", data=data)


async def report_progress(status: str, data: dict):
    async with ClientSession() as session:
        url = f"http://{API_HOST}:{API_PORT}/messages"
        payload = {
            "msg_datetime": str(datetime.now()),
            "msg_status": "delivered",
            "mailing_id": data.get("mailing_id"),
            "client_id": data.get("client_id")
        }
        async with session.post(url=url, json=payload) as response:
            print(await response.text())


class WorkerSettings:
    functions = [send_message]
    redis_settings = RedisSettings(host=REDIS_HOST, port=REDIS_PORT)
